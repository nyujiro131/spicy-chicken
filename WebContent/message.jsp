<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script src="./js/devjQuery-3.6.0.js"></script>
<script src="./js/jQuery.min.js"></script>
<script src="./js/main.js"></script>

</head>
<body>
	<div class="main-contents">
		<div class="header">
			<a href="./" class="messageHome"><b>ホーム</b></a>
		</div>
		<div class="form-area"></div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
				<c:remove var="errorMessages" scope="session" />
				</c:if>
			<form action="message" method="post">
<b>タイトル</b>
				<input name="title" id="title" class = "create-title" value="${message.title}" /><br>
				<b>カテゴリ</b>
				<input name="category" id="category" class = "create-category" value="${message.category}" /><br><br><b>投稿内容</b><br>
				<textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${message.text}" /></textarea>
				<br /> <input type="submit" class="btn" value="投稿"> &nbsp
				<span class="cnt_area"><span class="now_cnt">0</span> / 1000</span>
			</form>
	</div>
	</div>