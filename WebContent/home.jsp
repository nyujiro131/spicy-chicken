<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SpicyChicken</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script src="./js/devjQuery-3.6.0.js"></script>
<script src="./js/jQuery.min.js"></script>
<script src="./js/main.js"></script>
<script type="text/javascript" src="./js/paginathing.js"></script>

<script>
	function check() {

		if (window.confirm('削除してよろしいですか？')) {

			return true;
		} else {
			window.alert('キャンセルされました');
			return false;
		}

	}
</script>
<script type="text/javascript">
    		jQuery(document).ready(function($) {
				$('.messages').paginathing({
					perPage : 4,
					firstLast : true,
					prevText : '前へ',
					nextText : '次へ',
					activeClass : 'active',
					containerClass: 'panel-footer'
				})
			});
		</script>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
			</c:if>

			<c:if test="${ not empty loginUser }">
				<a href="message" class="messageLink"><b>新規投稿</b></a>
				<a href="./" class="homeLink"><b>ホーム</b></a>

				<!--<c:if test="${loginUser.branchId == 1 and loginUser.departmentId== 4}">-->
				<a href="management"class="manegementLink"><b>ユーザー管理</b></a>
				<!-- </c:if> -->
				<a href="logout"class="logoutLink"><b>ログアウト</b></a>
			</c:if>
		</div>
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					<h3>
						@
						<c:out value="${loginUser.account}" />
					</h3>
				</div>
			</div>
			<form action="" method="get">
				<div class="accessDate">
					最終ログイン日時：<c:out value="${accessTime}" />
				</div>
			</form>
		</c:if>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="" method="get">
			<div class="narrow">

				<br>日付<input type="date" id="start" name="start"
					value="${startDate}" /> ～
					 <input type="date" id="end" name="end" value="${endDate}" /> <br>
					カテゴリー<input type="text" name="category" id="category" />
					 <input type="submit" value="絞込">
			</div>
		</form>

		<div class="messages">
			<c:forEach items="${messages}" var="message">
			<div class ="messages-items">
				<div class="message">
					<div class="account-name">
						<br> <div class="account">
						<b><c:out value="${message.name}" /></b>&nbsp
						<span class="accountName">
						@<c:out value="${message.account}" /></span></div>
					</div>
					<span class="title-font">タイトル</span><br>
					<div class="message-title">
						<c:out value="${message.title}" />
					</div>
					<span class="category-font">カテゴリー</span><br>
					<div class="message-category">
						<c:out value="${message.category}" />
						<br>
					</div>
					<span class="text-font">本文</span><br>
					<div class="message-text">
						<c:set var="newLine" value="\\r\\n" />
						<c:forEach items='${ message.text.split(newLine) }' var="text">
							<c:out value="${text}" />
							<br>
						</c:forEach>
					</div>
					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>

					<form action="like" method="post">
						<input name="id" value="${message.id}" id="id" type="hidden" />
						<input name="like" value="${message.likeCount}" id="like" type="hidden" />
						<input type="submit" id="like" value="いいね">&nbsp &nbsp
						<c:out value="${message.likeCount}"/>
					</form>
					<c:if
						test="${ (not empty loginUser)and(loginUser.account == message.account) }">
						<form action="deleteMessage" method="post"
							onSubmit="return check()">
							<input name="id" value="${message.id}" id="id" type="hidden" />
							<input type="submit" id="delete" value="削除">
						</form>
					</c:if>
				</div>

				<div class="comments">
					<c:forEach items="${comments}" var="comment">
						<c:if test="${message.id == comment.messageId}">
							<div class="account-name">
								<br> <div class="account">
								<b><c:out value="${comment.name}" /></b>&nbsp
								<span class="accountName">
								@<c:out value="${comment.account}" /></span></div>
							</div>

							<div class="comment-text">
								<c:forEach items='${ comment.text.split(newLine) }' var="text">
									<c:out value="${text}" />
									<br>
								</c:forEach>
							</div>
							<div class="date">
								<fmt:formatDate value="${comment.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>

							<c:if test="${ (not empty loginUser)and(loginUser.account == comment.account) }">
								<form action="deleteComment" method="post"
									onSubmit="return check()">
									<input name="id" value="${comment.id}" id="id" type="hidden" />
									<input type="submit" id="delete" value="削除">
								</form>
							</c:if>
						</c:if>
					</c:forEach>
				</div>
					<c:if test="${not empty loginUser.id }">
						<form action="comment" method="post">
							<textarea name="comment" cols="100" rows="5" class="tweet-box"></textarea>
							<input name="id" value="${message.id}" id="id" type="hidden" />
							<br /> <input type="submit" class="btn" value="返信">&nbsp(500文字まで)
						</form>
					</c:if>
			</div>
			</c:forEach>
		</div>


		<div class="copyright"><b>Copyright(c)SpicyChicken</b></div>
	</div>
</body>
</html>