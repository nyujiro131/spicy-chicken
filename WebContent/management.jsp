<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.account}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/devjQuery-3.6.0.js"></script>
<script type="text/javascript" src="./js/jQuery.min.js"></script>
<script src="./js/main.js"></script>

</head>
<body>
	<div class="main-contents">
		<div class="header">
			<a href="./" class="manegementHome"><b>ホーム</b></a>
			<a href="signup" class="manegementUser"><b>ユーザー新規登録</b></a><br>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="management" method="get">
			<div class="narrow">
				<br>支社
				<select name="branch">
					<option value="0" selected>全て</option>
					<c:forEach items="${branches}" var="branch">
						<option value="${branch.id}">${branch.name}</option>
					</c:forEach>
				</select>
				部署
				 <select name="department">
				 	<option value="0" selected>全て</option>
					<c:forEach items="${departments}" var="department">
						<option value="${department.id}">${department.name}</option>
					</c:forEach>
				</select>
				 <input type="submit" value="検索">
			</div>
		</form>
		<c:forEach items="${user}" var="user"><br>
			アカウント&nbsp;：&nbsp;<c:out value="${user.account}" /><br>
			名前&nbsp;：&nbsp;<c:out value="${user.name}" /><br>
			支社&nbsp;：&nbsp;<c:out value="${branchMap[user.branchId]}" /><br>
			部署&nbsp;：&nbsp;<c:out value="${departmentMap[user.departmentId]}" /><br>
			アカウント復活停止状態&nbsp;：&nbsp;
			<c:choose>
				<c:when test="${user.isStopped == '1'}">valid</c:when>
				<c:when test="${user.isStopped == '0'}">invalid</c:when>
			</c:choose>

			<form action="setting" method="get">
				<input name="id" value="${user.id}" id="id" type="hidden" />
				<input type="submit" value="編集" id=editbutton>
			</form>
			<script type="text/javascript">
				function check(){
					if(window.confirm("本当に情報更新しますか？")){
						return true;
					}else{ // 「キャンセル」時の処理
						window.alert('キャンセルされました'); // 警告ダイアログを表示
						return false; // 送信を中止
					}
				}
			</script>
			<c:if test="${user.branchId !='1' and user.departmentId != '4' }">
			<form name="stops" action="stop" method="post" >
				<input name="id" value="${user.id}" id="id" type="hidden" />
				<input name="stop" value="${user.isStopped}" id="stop" type="hidden" />

				<c:if test="${user.isStopped == '1'}">
					<input type="submit" value="停止" name="btn" onClick="return check()">
				</c:if>
				<c:if test="${user.isStopped == '0'}">
					<input type="submit" value="復活" name="btn" onClick="return check()">
				</c:if>
			</form>

			<form name="deleteUser" action="deleteUser" method="post" >
				<input name="id" value="${user.id}" id="id" type="hidden" />
				<input type="submit" value="削除" name="btn" onClick="return check()">
			</form>
			</c:if>
		</c:forEach>

		<div class="copyright"><b>Copyright(c)SpicyChicken</b></div>
	</div>
</body>
</html>