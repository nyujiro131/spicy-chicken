<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script src="./js/devjQuery-3.6.0.js"></script>
<script src="./js/jQuery.min.js"></script>
<script src="./js/main.js"></script>

</head>
<body>
	<div class="main-contents">
		<div class="header">
			<a href="management">ユーザー管理</a>
		</div>

		<c:if test="${ not empty errorMessages }">
	        <div class="errorMessages">
	        <ul>
	        	<c:forEach items="${errorMessages}" var="errorMessage">
	            	<li><c:out value="${errorMessage}" />
	            </c:forEach>
	        </ul>
	        </div>
	        <c:remove var="errorMessages" scope="session" />
	     </c:if>

		<form action="signup" method="post"><br />
			<label for="account">アカウント</label>
	        <input name="account" id="account"value="${user.account}" /><br />

	        <label for="password">パスワード</label>
	        <input name="password" id="password" type="password" /> <br />

	        <label for="confirm">確認用パスワード</label>
	        <input name="confirm" type="password" id="confirm" /> <br />

	        <label for="name">名前</label>
	        <input name="name" id="name" value="${user.name}" /> <br />

			<label for="branch">支社</label>
			<select name="branchList" id="branchList" onchange="choice(this.value)">
				<option disabled selected>選択</option>
				<option id="1" value="1">本社</option>
				<option id="2" value="2">A支社</option>
				<option id="3" value="3">B支社</option>
				<option id="4" value="4">C支社</option>

			</select><br />


				<c:choose>
				<c:when test="${user.branchId == 1}">
				<label for="department">部署</label>
				<select name="departmentList"  id="departmentList" >
					<option disabled selected>選択</option>
					<option id="4" value="総務人事部">総務人事部</option>
					<option id="3" value="情報管理部">情報管理部</option>
				</select><br />
				</c:when>

				<c:otherwise>
				<label for="department">部署</label>
				<select name="departmentList"  id="departmentList"  >
					<option disabled selected>選択</option>
					<option id="2" value="技術部">技術部</option>
					<option id="1" value="営業部">営業部</option>
				</select><br />
				</c:otherwise>
				</c:choose>
				<br />

				<script type="text/javascript">
			window.onload = function() {
				branchId ="${user.branchId}";
				departmentId ="${user.departmentId}";
				var branchList = document.getElementById("branchList");
				var departmentList = document.getElementById("departmentList");

				for (var i = 0; i < branchList.length; i++) {
					if (branchId == parseInt(branchList[i].value)) {
						branchList[i].selected = true;
						break;
					}

				}
				for (var i = 0; i < departmentList.length; i++) {
					if (departmentId == parseInt(departmentList[i].id) ) {
						departmentList[i].selected = true;
						break;
					}
				}

			}
			</script>

	         <input type="submit" value="登録" /> <br />
	    </form>

	            <div class="copyright"><b>Copyright(c)SpicyChicken</b></div>
	</div>
	</body>
</html>