package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    password, ");
            sql.append("    name, ");
            sql.append("    branch_id, ");
            sql.append("    department_id, ");
            sql.append("    is_stopped, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // account
            sql.append("    ?, ");                                  // name
            sql.append("    ?, ");                                  // email
            sql.append("    ?, ");                                  // password
            sql.append("    ?, ");
            sql.append("    1, ");
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public User select(Connection connection, int userId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, userId);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User select(Connection connection, String account, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

            ps = connection.prepareStatement(sql);

            ps.setString(1, account);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
        public List<User> select(Connection connection) {

            PreparedStatement ps = null;
            try {
                String sql = "SELECT * FROM users";

                ps = connection.prepareStatement(sql);

                ResultSet rs = ps.executeQuery();

                List<User> users = toUsers(rs);
                if (users.isEmpty()) {
                    return null;
                } else {
                    return users;
                }
//                } else if (2 <= users.size()) {
//                   throw new IllegalStateException("ユーザーが重複しています");
            } catch (SQLException e) {
                throw new SQLRuntimeException(e);
            } finally {
                close(ps);
            }
        }



    public User userSelect(Connection connection, String account) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? ";

            ps = connection.prepareStatement(sql);

            ps.setString(1, account);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUsers(ResultSet rs) throws SQLException {

        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setAccount(rs.getString("account"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setBranchId(rs.getInt("branch_id"));
                user.setDepartmentId(rs.getInt("department_id"));
                user.setIsStopped(rs.getInt("is_stopped"));
                user.setCreatedDate(rs.getTimestamp("created_date"));
                user.setUpdatedDate(rs.getTimestamp("updated_date"));

                users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }

    public void update(Connection connection, User user ) {

        PreparedStatement ps = null;
        String password = user.getPassword();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("    account = ?, ");
            if (!StringUtils.isEmpty(password)) {
            	sql.append("    password = ?, ");
            }
            sql.append("    name = ?, ");
            sql.append("    branch_id = ?, ");
            sql.append("    department_id = ?, ");
            sql.append("    updated_date = CURRENT_TIMESTAMP ");
            sql.append("WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());

            if (!StringUtils.isEmpty(password)) {
            	ps.setString(2, user.getPassword());
            	ps.setString(3, user.getName());
            	ps.setInt(4, user.getBranchId());
            	ps.setInt(5, user.getDepartmentId());
            	ps.setInt(6, user.getId());
            }else {
            	ps.setString(2, user.getName());
            	ps.setInt(3, user.getBranchId());
            	ps.setInt(4, user.getDepartmentId());
            	ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, String userId,int i) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("    is_stopped = ? ");
            sql.append("WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, i);
            ps.setString(2, userId);

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> userSelect(Connection connection, String idBranch,String idDepartment) {

        PreparedStatement ps = null;
        try {

        	int branchId = Integer.parseInt(idBranch);
        	int departmentId = Integer.parseInt(idDepartment);

        	StringBuilder sql = new StringBuilder();
        		sql.append("SELECT * FROM users ");

        		if(branchId != 0 && departmentId != 0) {
        			sql.append("WHERE branch_id = ? ");
        			sql.append("AND department_id = ?");
        		}
        		if(branchId == 0 && departmentId != 0) {
        			sql.append("WHERE department_id = ?");
        		}
        		if(departmentId == 0 && branchId != 0) {
        			sql.append("WHERE branch_id = ?");
        		}

            ps = connection.prepareStatement(sql.toString());

            if(branchId != 0 && departmentId != 0) {
            	ps.setInt(1, branchId);
            	ps.setInt(2, departmentId);
    		}
            if(branchId == 0 && departmentId != 0) {
            	ps.setInt(1, departmentId);
            }
            if(departmentId == 0 && branchId != 0) {
            	ps.setInt(1, branchId);
            }

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else {
                return users;
            }
        } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
  }


	public void UserDelete(Connection connection, int id) {
		PreparedStatement ps = null;

	    try {
	      StringBuilder sql = new StringBuilder();
	            sql.append("DELETE  FROM users WHERE id = ?  ");

	      ps = connection.prepareStatement(sql.toString());

	      ps.setInt(1, id);

	      ps.executeUpdate();

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }

	}

}