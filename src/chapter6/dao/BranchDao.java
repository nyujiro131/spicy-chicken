package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Branch;
import chapter6.exception.SQLRuntimeException;

public class BranchDao {

  public List<Branch> select(Connection connection) {

	    PreparedStatement ps = null;

	    try {
	      StringBuilder sql = new StringBuilder();
	            sql.append("SELECT  *  ");
	            sql.append("    FROM branches ");

	      ps = connection.prepareStatement(sql.toString());

	      ResultSet rs = ps.executeQuery();

	      List<Branch> branches = toBranches(rs);

	      if (branches.isEmpty()) {
              return null;
          } else {
              return branches;
          }

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
  }

  private List<Branch> toBranches(ResultSet rs) throws SQLException {

      List<Branch> branches = new ArrayList<Branch>();
      try {
          while (rs.next()) {
              Branch branch = new Branch();
              branch.setId(rs.getInt("id"));
              branch.setName(rs.getString("name"));

              branches.add(branch);
          }
          return branches;
      } finally {
          close(rs);
      }
  }
}