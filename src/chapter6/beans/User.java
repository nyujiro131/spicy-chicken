package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    private int id;
    private String account;
    private String name;
    private String password;
    private String confirm;
    private int branchId;
    private int departmentId;
    private int isStopped;
    private Date createdDate;
    private Date updatedDate;

    // getter/setterは省略されているので、自分で記述しましょう。
    public int getId() {
    	return id;
    }

    public void setId(int id) {
    	this.id = id;
    }

    public String getAccount() {
    	return account;
    }

    public void setAccount(String account) {
    	this.account = account;
    }

    public String getName() {
    	return name;
    }

    public void setName(String name) {
    	this.name = name;
    }

    public String getPassword() {
    	return password;
    }

    public void setPassword(String password) {
    	this.password = password;
    }

    public Date getCreatedDate() {
    	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
    	return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public int getIsStopped() {
		return isStopped;
	}

	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String comfirm) {
		this.confirm = comfirm;
	}

}