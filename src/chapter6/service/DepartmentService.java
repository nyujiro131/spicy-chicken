package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Department;
import chapter6.dao.DepartmentDao;

public class DepartmentService {

    public List<Department> select() {

        Connection connection = null;
        try {
            connection = getConnection();
            List<Department> department = new DepartmentDao().select(connection);
            commit(connection);
            return department;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}