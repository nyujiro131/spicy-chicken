package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.sun.jmx.snmp.Timestamp;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public  Message MessageSelect(int id) {

        Connection connection = null;
        try {
            connection = getConnection();
           Message message = new MessageDao().MessageSelect(connection, id);
            commit(connection);
            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Message message) {
        Connection connection = null;
        try {

            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void MessageDelete(int id) {
        Connection connection = null;
        try {

            connection = getConnection();
            new MessageDao().MessageDelete(connection, id);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public List<UserMessage> select(String startDate, String endDate,String category,String userId) {
    	final int LIMIT_NUM = 1000;
    	Connection connection = null;
    	try {
    		if(!StringUtils.isEmpty(startDate)) {
    			startDate += " 00:00:00";
        	}else{
        		startDate = "2020-01-01 00:00:00";
        	}

    		if(!StringUtils.isEmpty(endDate)) {
    			endDate += " 23:59:59";
        	}else {
        		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
       		 	SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       		 	endDate = date.format(new Date());
        	}

    		if(StringUtils.isEmpty(category)) {
    			category = null;
    		}

    		Integer id = null;
    		if(!StringUtils.isEmpty(userId)) {
    		      id = Integer.parseInt(userId);
    		}

    		connection = getConnection();
    		List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, startDate, endDate,category,id);
    		commit(connection);
    		return messages;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);

    	}

    }

    public void update(int count,String messageId) {
        Connection connection = null;
        try {

            connection = getConnection();
            new MessageDao().update(connection, count, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}