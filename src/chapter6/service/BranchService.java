package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Branch;
import chapter6.dao.BranchDao;

public class BranchService {

    public List<Branch> select() {

        Connection connection = null;
        try {
            connection = getConnection();
            List<Branch> branch = new BranchDao().select(connection);
            commit(connection);
            return branch;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}