package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {

    public void insert(User user) {
        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User select(int userId) {

        Connection connection = null;
        try {
            // パスワード暗号化
            connection = getConnection();
            User user = new UserDao().select(connection, userId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

        public User select(String account, String password) {

            Connection connection = null;
            try {
                // パスワード暗号化
                String encPassword = CipherUtil.encrypt(password);

                connection = getConnection();
                User user = new UserDao().select(connection, account, encPassword);
                commit(connection);

                return user;
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }

        public List<User> select() {

            Connection connection = null;
            try {
                connection = getConnection();
                List<User> user = new UserDao().select(connection);
                commit(connection);

                return user;
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }

        public User userSelect(String account) {

            Connection connection = null;
            try {
                connection = getConnection();
                User user = new UserDao().userSelect(connection, account);
                commit(connection);

                return user;
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }

        public void update(User user) {

            Connection connection = null;
            try {
            	String password = user.getPassword();
                // パスワード暗号化
            	if (!StringUtils.isEmpty(password)) {
            		String encPassword = CipherUtil.encrypt(user.getPassword());
                    user.setPassword(encPassword);
                }

                connection = getConnection();
                new UserDao().update(connection, user);
                commit(connection);
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }


        public void update(String userId,int stopNum) {

            Connection connection = null;
            try {
            	 connection = getConnection();
                 commit(connection);

                 int i ;
                 if(stopNum==1) {
                 	i = 0;
                 }else {
                 	i = 1;
                 }

                connection = getConnection();
                new UserDao().update(connection, userId,i);
                commit(connection);
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }

        public List<User> userSelect(String idBranch , String idDepartment) {

            Connection connection = null;
            try {
                // パスワード暗号化

                connection = getConnection();
                List<User> user = new UserDao().userSelect(connection, idBranch,idDepartment);
                commit(connection);

                return user;
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }

		public void UserDelete(int id) {
			Connection connection = null;
	        try {

	            connection = getConnection();
	            new UserDao().UserDelete(connection, id);
	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}