package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Branch;
import chapter6.beans.Department;
import chapter6.beans.User;
import chapter6.service.BranchService;
import chapter6.service.DepartmentService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	List<Branch> branches = new BranchService().select();
    	List<Department> departments = new DepartmentService().select();

    	HttpSession session = request.getSession();
    	session.removeAttribute("user");
    	session.setAttribute("branches", branches);
    	session.setAttribute("departments", departments);
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();
        User user = getUser(request);
        if (!isValid(user, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }
        String account = user.getAccount();
        User usersAccount = new UserService().userSelect(account);

        if(usersAccount != null ){
        	errorMessages.add("ユーザが重複しています");

        	request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }
        new UserService().insert(user);
        response.sendRedirect("./");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	Map<String,Integer> branch = new HashMap<String,Integer>();
	    	branch.put("1",1);
	    	branch.put("2",2);
	    	branch.put("3",3);
	    	branch.put("4",4);

	    Map<String,Integer> department = new HashMap<String,Integer>();
    		department.put("営業部",1);
    		department.put("技術部",2);
    		department.put("情報管理部",3);
    		department.put("総務人事部",4);

        User user = new User();
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setConfirm(request.getParameter("confirm"));
        user.setName(request.getParameter("name"));
        System.out.println(request.getParameter("branchList"));
        System.out.println(request.getParameter("departmentList"));
       if(!StringUtils.isEmpty(request.getParameter("branchList"))) {
    	   user.setBranchId(branch.get(request.getParameter("branchList")));
       }
       if(!StringUtils.isEmpty(request.getParameter("departmentList"))) {
    	   user.setDepartmentId(department.get(request.getParameter("departmentList")));
       }
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages) {

        String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();
        String confirm = user.getConfirm();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();
        System.out.println(branchId);
        System.out.println(departmentId);
        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if ( 6 > account.length()) {
        	errorMessages.add("アカウント名は6文字以上で入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }else if ( 6 > password.length()) {
        	errorMessages.add("パスワードは6文字以上で入力してください");
        } else if (20 < password.length()) {
            errorMessages.add("パスワードは20文字以下で入力してください");
        }

        if(!password.equals(confirm)) {
        	errorMessages.add("パスワードが一致していません");
        }

        if (StringUtils.isEmpty(name)) {
            errorMessages.add("名前を入力してください");
        }else if (10 < name.length()) {
            errorMessages.add("名前は10文字以下で入力してください");
        }
        if(branchId == 0) {
        	errorMessages.add("支社を選択してください");
        }
        if(departmentId == 0) {
        	errorMessages.add("部署を選択してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
