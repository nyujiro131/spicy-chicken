package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		int id = Integer.parseInt(request.getParameter("id"));
		String text = request.getParameter("comment");
		if (!isValid(text, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		if (!NgwordChecker(text, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			request.setAttribute("text", text);
			response.sendRedirect("./");
			return;
		}

		Comment comment = new Comment();
		comment.setText(text);

		User user = (User) session.getAttribute("loginUser");
		comment.setUserId(user.getId());
		comment.setMessageId(id);
		new CommentService().commentInsert(comment);

		response.sendRedirect("./");

	}

	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (500 < text.length()) {
			errorMessages.add("500文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

	private boolean NgwordChecker (String text, List<String> errorMessages){

		String value[] = {"返信"};
		String contents[] = {text};//判定する文字列
		for(int o = 0;o<1;o++) {//タイトル(最小、最大)、紹介文(最小、最大)

			//禁止文字種を増やしたい場合は、この配列内に要素を追加してください
			String word[] = {"悪口1","悪口2","悪口3","悪口4"};

			for(String i:word){
				StringBuilder sb = new StringBuilder();
				sb.append("^(.*");
				sb.append(i);
				sb.append(").*");
				Pattern p = Pattern.compile(sb.toString());
				Matcher m = p.matcher(contents[o]);

				//禁止文字が含まれているか判定する
				if(m.find()) {
					errorMessages.add(value[o]+"に禁止文字が含まれています。");
					break;
				}
			}
		}
		if (errorMessages.size() != 0) {

			return false;
		}
		return true;
	}
}