package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String idPass = request.getParameter("id");
		if (StringUtils.isEmpty(idPass) || !idPass.matches("^[0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました");

	    	session.setAttribute("errorMessages", errorMessages);
	    	response.sendRedirect("./");
	        return;
	    }

	        int userId = Integer.parseInt(idPass);
	        User user = new UserService().select(userId);

	        if (user == null) {
				errorMessages.add("不正なパラメータが入力されました");
				session.setAttribute("errorMessages", errorMessages);
				response.sendRedirect("./");
				return;
			}

	    	Map<Integer,String> branch = new HashMap<Integer,String>();
		    	branch.put(1,"本社");
		    	branch.put(2,"A支社");
		    	branch.put(3,"B支社");
		    	branch.put(4,"C支社");

		    Map<Integer,String> department = new HashMap<Integer,String>();
		    	department.put(1,"営業部");
		    	department.put(2,"技術部");
		    	department.put(3,"情報管理部");
		    	department.put(4,"総務人事部");

			session.setAttribute("branch", branch);
			session.setAttribute("department", department);
	        session.setAttribute("user", user);
	        request.getRequestDispatcher("setting.jsp").forward(request, response);


	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();
        User selectUser = (User) session.getAttribute("user");
        User user = getUser(request);

        if (isValid(user, errorMessages)) {
            try {
            	String account = user.getAccount();
                String selectAccount = selectUser.getAccount();
                User usersAccount = new UserService().userSelect(account);

                if(usersAccount != null && !selectAccount.equals(usersAccount.getAccount())){
                	errorMessages.add("ユーザが重複しています");

                	request.setAttribute("errorMessages", errorMessages);
                    request.setAttribute("user", user);
                    request.getRequestDispatcher("setting.jsp").forward(request, response);
                    return;
                }
                new UserService().update(user);
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

        if (errorMessages.size() != 0) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }



        response.sendRedirect("management");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

    	Map<String,Integer> department = new HashMap<String,Integer>();
			department.put("営業部",1);
			department.put("技術部",2);
			department.put("情報管理部",3);
			department.put("総務人事部",4);

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("id")));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setConfirm(request.getParameter("confirm"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchList")));
        if(StringUtils.isEmpty(request.getParameter("departmentList"))) {
     	   user.setDepartmentId(0);
     	  return user;
        }
        if(request.getParameter("departmentList").equals("4")) {
        	user.setDepartmentId(Integer.parseInt(request.getParameter("departmentList")));
        }else {
        	user.setDepartmentId(department.get(request.getParameter("departmentList")));
        }

        return user;
    }

    private boolean isValid(User user, List<String> errorMessages) {

        String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();
        String confirm = user.getConfirm();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        }else if ( 6 > account.length()) {
        	errorMessages.add("アカウント名は6文字以上で入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        }

        if (!StringUtils.isEmpty(password)) {
        	if ( 6 > password.length()) {
        		errorMessages.add("パスワードは6文字以上で入力してください");
        	} else if (20 < password.length()) {
        		errorMessages.add("パスワードは20文字以下で入力してください");
        	}
        }

        if(!password.equals(confirm)) {
        	errorMessages.add("パスワードが一致していません");
        }

        if (StringUtils.isEmpty(name)) {
            errorMessages.add("名前を入力してください");
        }else if (10 < name.length()) {
            errorMessages.add("名前は10文字以下で入力してください");
        }

        if(departmentId == 0) {
        	errorMessages.add("部署を選択してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
