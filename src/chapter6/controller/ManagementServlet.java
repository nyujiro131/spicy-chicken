package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Branch;
import chapter6.beans.Department;
import chapter6.beans.User;
import chapter6.service.BranchService;
import chapter6.service.DepartmentService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	List<String> errorMessages = new ArrayList<String>();
    	List<Branch> branches = new BranchService().select();
    	List<Department> departments = new DepartmentService().select();

        Map<Integer,String> branchMap = new HashMap<Integer,String>();

        for(int n = 0; n < branches.size(); n++) {
            int branchId = branches.get(n).getId();
            String branchName = branches.get(n).getName();

            branchMap.put(branchId, branchName);
        }

        Map<Integer,String> departmentMap = new HashMap<Integer,String>();

        for(int n = 0; n < departments.size(); n++) {
            int departmentId = departments.get(n).getId();
            String departmentName = departments.get(n).getName();

            departmentMap.put(departmentId, departmentName);
        }

        String idBranch = request.getParameter("branch");
    	String idDepartment = request.getParameter("department");

    	if(idBranch == null) {
   		 	idBranch = "0";
    	}

    	if(idDepartment == null) {
    		idDepartment = "0";
    	}

    	if(idBranch.equals("1")) {
    		if(!(idDepartment.equals("0")) && !(idDepartment.equals("4")) && !(idDepartment.equals("3"))) {
	    		errorMessages.add("正しく入力してください");
	    		request.setAttribute("errorMessages", errorMessages);

	    		idBranch = "0";
	    		idDepartment = "0";
    		}
    	}

    	if(!(idBranch.equals("1")) && !(idBranch.equals("0"))) {
    		if(idDepartment.equals("4") || idDepartment.equals("3")) {
    			errorMessages.add("正しく入力してください");
    			request.setAttribute("errorMessages", errorMessages);

	    		idBranch = "0";
	    		idDepartment = "0";
    		}
    	}

        List<User> user = new UserService().userSelect(idBranch,idDepartment);

        request.setAttribute("departments",departments);
        request.setAttribute("branchMap",branchMap);
        request.setAttribute("departmentMap",departmentMap);
        request.setAttribute("user", user);
        request.getRequestDispatcher("management.jsp").forward(request, response);
    }
}