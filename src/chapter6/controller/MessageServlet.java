package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;


@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("message.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		Message message = new Message();
		message.setText(text);
		message.setCategory(category);
		message.setTitle(title);

		if (!isValid(title, category, text, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}

		if (!NgwordChecker(title, category, text, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}


		User user = (User) session.getAttribute("loginUser");
		message.setUserId(user.getId());


		new MessageService().insert(message);
		response.sendRedirect("./");
	}


	private boolean isValid(String title, String category,String text, List<String> errorMessages) {



		if (StringUtils.isBlank(title)){
			errorMessages.add("件名を入力してください");
		}else if(30 < title.length()){
			errorMessages.add("件名は30文字以下で入力してください");
		}else {

		}
		if(StringUtils.isBlank(category)){
			errorMessages.add("カテゴリを入力してください");
		}else if (10 < category.length()){
			errorMessages.add("カテゴリは10文字以下で入力してください");
		} else{

		}
		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (1000 < text.length()) {
			errorMessages.add("メッセージは1000文字以下で入力してください");
		}else {

		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
	private boolean NgwordChecker (String title, String category,String text, List<String> errorMessages){

		String value[] = {"タイトル","カテゴリー","新規投稿"};
		String contents[] = {title,category,text};//判定する文字列
		for(int o = 0;o<3;o++) {//タイトル(最小、最大)、紹介文(最小、最大)

			//禁止文字種を増やしたい場合は、この配列内に要素を追加してください
			String word[] = {"悪口1","悪口2","悪口3","悪口4"};

			for(String i:word){
				StringBuilder sb = new StringBuilder();
				sb.append("^(.*");
				sb.append(i);
				sb.append(").*");
				Pattern p = Pattern.compile(sb.toString());
				Matcher m = p.matcher(contents[o]);

				//禁止文字が含まれているか判定する
				if(m.find()) {
					errorMessages.add(value[o]+"に禁止文字が含まれています。");
					break;
				}
			}
		}
		if (errorMessages.size() != 0) {

			return false;
		}
		return true;
	}

}
