package chapter6.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.setCharacterEncoding("UTF-8");

		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest)request).getSession();
		HttpServletResponse res = (HttpServletResponse)response;

		boolean isShowMessageForm = false;

		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}
		if (user == null) {
			errorMessages.add("ログインしてください！");
			session.setAttribute("errorMessages", errorMessages);
			res.sendRedirect("./login");
			return;
		}

		String userId = request.getParameter("user_id");
		String startDate = request.getParameter("start");
		String endDate = request.getParameter("end");
		String category = request.getParameter("category");

	    long accessTime = session.getLastAccessedTime();
	    Date accessDate = new Date(accessTime);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	    String strDate = sdf.format(accessDate);


		List<UserMessage> messages = new MessageService().select(startDate,endDate,category,userId);
		List<UserComment> comments = new CommentService().select();



		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("category", category);
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("accessTime", strDate);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/home.jsp").forward(request, response);

	}
}