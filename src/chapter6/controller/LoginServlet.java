package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
        HttpSession session = request.getSession();
        session.removeAttribute("errorMessages");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String account = request.getParameter("account");
        String password = request.getParameter("password");
        User user = new UserService().select(account, password);

        if (user == null) {
            List<String> errorMessages = new ArrayList<String>();

            if (StringUtils.isBlank(account)) {
                errorMessages.add("アカウントが入力されていません");
            }
            if (StringUtils.isBlank(password)) {
                errorMessages.add("パスワードが入力されていません");
            }else if (user == null) {
        	errorMessages.add("アカウントまたはパスワードが誤っています");

            }
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        if(user.getIsStopped()==0) {
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages.add("アカウントが停止されています");
        	 request.setAttribute("errorMessages", errorMessages);
             request.getRequestDispatcher("login.jsp").forward(request, response);
             return;
        }

        HttpSession session = request.getSession();
        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }
}