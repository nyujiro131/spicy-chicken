package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
@WebFilter(urlPatterns={"/setting","/management","/signup"})
public class ManagementFilter implements Filter{
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException{


		// セッションが存在しない場合NULLを返す
		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		int branchId = loginUser.getBranchId();
		int departmentId = loginUser.getDepartmentId();


		if(branchId == 1 && departmentId == 4 ){
			// セッションがNULLでなければ、通常どおりの遷移
			chain.doFilter(request, response);
		}else{
			// セッションがNullならば、ログイン画面へ飛ばす
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("権限がありません");
			session.setAttribute("errorMessages", errorMessages);
	        ((HttpServletResponse)response).sendRedirect("./");
		}

	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}