package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
@WebFilter("/*")
public class LoginFilter implements Filter{
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException{


		// セッションが存在しない場合NULLを返す
		List<String> errorMessages = new ArrayList<String>();
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		String Url = req.getServletPath();


		if(loginUser == null && !Url.equals("/login")&& !Url.contains("css")&& !Url.contains("js")){
			errorMessages.add("ログインしてください！");
			session.setAttribute("errorMessages", errorMessages);
			res.sendRedirect("./login");
			return;

		}

		chain.doFilter(request, response);

	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}